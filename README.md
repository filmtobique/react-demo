## Name
React-Demo

## Description
A few example projects using the react framework.

## Project status

- tic-tac-toe

    - sources:

        https://reactjs.org/tutorial/tutorial.html

    - demonstrating 
        ES6,
        arrow functions, 
        classes, 
        let, 
        and 
        const statements

    - expected result:

        - Lets you play tic-tac-toe
        - Indicates when a player has won the game
        - Stores a game’s history as a game progresses
        - Allows players to review a game’s history 
            and see previous versions of a game’s board
        - Form input clears automatically on submit

- todolist

    - sources:

        https://www.freecodecamp.org/news/react-tutorial-build-a-project/

    - demonstrating

        hooks, 
        state management, 
        forms, 
        JSX elements, 
        components, 
        props, 
        styling, 
        and 
        conditionals

    - expected result:

        - a working todo app that has full CRUD functionality that can create, read, update, and delete todos